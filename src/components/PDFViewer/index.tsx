// Import the styles
import {
  defaultLayoutPlugin,
  ToolbarProps,
} from "@react-pdf-viewer/default-layout";
import "@react-pdf-viewer/core/lib/styles/index.css";
import "@react-pdf-viewer/default-layout/lib/styles/index.css";
import { Worker, Viewer } from "@react-pdf-viewer/core";
import {
  PDFDocument,
  rgb,
  StandardFonts,
  PDFName,
  PDFHexString,
} from "pdf-lib";
import React, { useEffect, useRef, useState } from "react";
import type {
  ToolbarSlot,
  TransformToolbarSlot,
} from "@react-pdf-viewer/toolbar";

const testEle = {
  width: 100,
  height: 20,
  className: "ahihi",
};

const PDFViewer = () => {
  const [pdfBytes, setPdfBytes] = useState<any>(null);
  const [defaultScale, setDefaultScale] = useState<number>(2);
  const transform: TransformToolbarSlot = (slot: ToolbarSlot) => ({
    ...slot,
    DownloadMenuItem: () => <></>,
    EnterFullScreen: () => <></>,
    EnterFullScreenMenuItem: () => <></>,
    SwitchTheme: () => <></>,
    SwitchThemeMenuItem: () => <></>,
    Zoom: () => <></>,
    ZoomIn: () => <></>,
    ZoomOut: () => <></>,
  });

  const renderToolbar = (
    Toolbar: (props: ToolbarProps) => React.ReactElement,
  ) => <Toolbar>{renderDefaultToolbar(transform)}</Toolbar>;
  const defaultLayoutPluginInstance = defaultLayoutPlugin({
    renderToolbar,
    sidebarTabs: () => [],
  });
  const { renderDefaultToolbar } =
    defaultLayoutPluginInstance.toolbarPluginInstance;
  // Specify the path to your existing PDF file
  // const pdfUrl = "/my-pdf.pdf";
  const pdfUrl = "/my-pdf.pdf";

  async function renderPdf(pdfUrl) {
    const existingPdfBytes = await fetch(pdfUrl).then((res) =>
      res.arrayBuffer(),
    );
    // Load a PDFDocument from the existing PDF bytes
    const pdfDoc = await PDFDocument.load(existingPdfBytes);

    // Embed the Helvetica font
    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

    // Get the first page of the document
    const pages = pdfDoc.getPages();

    const firstPage = pages[0];
    // Get the width and height of the first page
    const { width, height } = firstPage.getSize();
    const pdfBytes = await pdfDoc.save();
    setPdfBytes(pdfBytes);
  }

  async function modifyPdf(x, y) {
    const existingPdfBytes = await fetch(pdfUrl).then((res) =>
      res.arrayBuffer(),
    );
    // Load a PDFDocument from the existing PDF bytes
    const pdfDoc = await PDFDocument.load(existingPdfBytes);
    // pdfDoc.addJavaScript(
    //   "submit",
    //   `
    //   app.alert("ahihi")
    // `,
    // );

    // Embed the Helvetica font
    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

    // Get the first page of the document
    const pages = pdfDoc.getPages();

    const firstPage = pages[0];
    // Get the width and height of the first page
    const { width, height } = firstPage.getSize();
    console.log({ width, height });
    //Get form instance
    const formInstance = pdfDoc.getForm();
    // Create Text Field
    const textField = formInstance.createTextField("custom");
    const submitButton = formInstance.createButton("submit_button");
    textField.addToPage(pdfDoc.getPage(0), {
      x: x / defaultScale,
      y: y / defaultScale,
      width: testEle.width / defaultScale,
      height: testEle.height / defaultScale,
      borderWidth: 0,
      backgroundColor: rgb(0, 1, 0),
    });

    // submitButton.addToPage("button", pdfDoc.getPage(0), {
    //   x: 30,
    //   y: 10,
    //   width: 200,
    //   height: 100,
    //   borderWidth: 0,
    //   backgroundColor: rgb(0, 0, 1),
    // });

    textField.setFontSize(12);
    textField.enableRequired();
    // Get all form field in pdf file
    const formFields = formInstance.getFields();
    // Serialize the PDFDocument to bytes (a Uint8Array)
    const pdfBytes = await pdfDoc.save();
    setPdfBytes(pdfBytes);
  }

  useEffect(() => {
    renderPdf(pdfUrl);
  }, [pdfUrl]);
  const viewerRef = useRef<any>(null);
  const containerRef = useRef<any>(null);

  const [position, setPosition] = useState<any>();
  const [pdfPosition, setPdfPosition] = useState<any>();

  async function handleOnclick(event: any) {
    // const viewerContainer = viewerRef.current.container;
    console.log("click");
    const viewerContainer = event.target;
    if (event.target.classList[0] !== testEle.className) {
      const rect = viewerContainer.getBoundingClientRect();
      const offsetX = event.clientX - rect.left;
      const offsetY = event.clientY - rect.top;

      const pageY = rect.height - offsetY - testEle.height / 2;
      setPosition({ x: offsetX, y: offsetY - testEle.height / 2 });
      setPdfPosition({ x: offsetX, y: pageY });
    }
  }

  function addInput() {}

  return (
    <div className={"pdf-container"}>
      {pdfBytes && (
        <>
          {/*<button onClick={addInput}>Add input</button>*/}
          <button
            onClick={() => {
              modifyPdf(pdfPosition.x, pdfPosition.y);
            }}
          >
            Modify
          </button>

          <Worker workerUrl={"/libraries/pdf-viewer-worker/pdf.worker.min.js"}>
            <div>
              <Viewer
                defaultScale={defaultScale}
                fileUrl={pdfBytes}
                plugins={[defaultLayoutPluginInstance]}
                renderPage={(props) => {
                  return (
                    <div
                      ref={containerRef}
                      onClick={handleOnclick}
                      style={{
                        width: "100%",
                        height: "100%",
                        zIndex: 10,
                        position: "relative",
                      }}
                    >
                      <div
                        className={testEle.className}
                        style={{
                          position: "absolute",
                          left: position?.x,
                          top: position?.y,
                          background: "red",
                          width: testEle.width,
                          height: testEle.height,
                          zIndex: 1,
                        }}
                      ></div>
                      {props.canvasLayer.children}
                    </div>
                  );
                }}
              ></Viewer>
            </div>
          </Worker>
        </>
      )}
    </div>
  );
};

export default PDFViewer;
